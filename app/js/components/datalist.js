/** 
 * @desc DataList - custom componet for list with selection and auto-filter.
 * @author Arunkumar passtoarun@gmail.com.
 */
export default class DataList {
    constructor(containerId, options, onSelect = null) {
        this.container = document.querySelector(`#${containerId}`);
        this.input = this.container.querySelector('input');
        this.list = this.container.querySelector('ul');;
        this.options = options;
        this.onSelect = onSelect;
        this.selectedValue = null;
        this.create();
        this.addListeners();
    }

    /**
     * @desc create - create the list based on the options.
     */
    create(filter = "") {
        const filterOptions = this.options.filter(
            d => filter === "" || d.text.includes(filter)
        );

        if (filterOptions.length === 0) {
            this.list.classList.remove("active");
        } else {
            this.list.classList.add("active");
        }

        this.list.innerHTML = filterOptions
            .map(o => `<li id=${o.value}>${o.text}</li>`)
            .join("");
    }

    /**
     * @desc addListeners - add user actions for click, input, blur events.
     */
    addListeners() {
        this.container.addEventListener("click", function(e) {
            if (e.target === this.input) {
                this.container.classList.toggle("active");
            } else if (e.target.id === "datalist-icon") {
                this.container.classList.toggle("active");
                this.input.focus();
            }
        }.bind(this));

        this.input.addEventListener("input", function(e) {
            if (!this.container.classList.contains("active")) {
                this.container.classList.add("active");
            }

            this.create(this.input.value);
        }.bind(this));

        /**
         * @desc addListeners - blur - hide list 
         */
        this.input.addEventListener("blur", function(e) {
            setTimeout(() => {
                this.container.classList.remove("active");
            }, 400);
        }.bind(this));

        /**
         * @desc invoke callback on selection along with data 
         */
        this.list.addEventListener("click", function(e) {
            if (e.target.nodeName.toLocaleLowerCase() === "li") {
                this.input.value = e.target.innerText;
                this.selectedValue = {
                    text: e.target.innerText,
                    id: e.target.id
                };
                this.container.classList.remove("active");
                if (this.onSelect) {
                    setTimeout(() => {
                        this.onSelect(this.selectedValue);
                    }, 0);
                }

            }
        }.bind(this));
    }
}