/** 
  * @desc Main module of the Let's Go flight booking application.
  * @author Arunkumar passtoarun@gmail.com.
  * @required SearchFactory, UtilService, SearchResultFactory.
*/


import * as SearchFactory from './service/search-factory.js';
import UtilService from './service/util-service.js';
import SearchResultFactory from './service/search-result-factory.js';

window.addEventListener('load', function() {

    /**
     * @desc createSearchBlock - carteate and capture input from user.
     * @return Object - search critera.
    */

    let getSearchPayload = SearchFactory.createSearchBlock();
    let searchBtn = document.querySelector('#searchBtn');
    let mainEle = this.document.querySelector('#app-main');

    /**
     * @desc validate the given search criteria.
     * @desc add mobile class main element on responsive layout.
     * @param Object searchPayLoad - search criteria.
     * @return Object - valid or invalid with error message.
    */
    searchBtn.addEventListener('click', function(e) {
        let searchPayLoad = getSearchPayload();

        let resultObj = UtilService.validateSearch(searchPayLoad);
        if (!resultObj.isValid) {
            alert(resultObj.msg);
            return;
        }
        mainEle.classList.toggle('mobile');
        SearchResultFactory.createSearchResult(searchPayLoad);

    }.bind(this));

    
    /**
     * @desc toggle view between seach menu and result.
     * @param Array list of elements to be binded for toggle between search and result screen .
    */
    let searchElements = ['nav-search-btn', 'menu-container']
    searchElements.forEach((id) => {
        let element = this.document.querySelector(`#${id}`);
        element.addEventListener('click', function(e) {
            if (searchElements.indexOf(e.target.id) === -1) {
                return;
            }
            mainEle.classList.toggle('mobile');
        });
    });

});