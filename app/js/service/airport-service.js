/** 
 * @desc Airport details - mock data for airport details.
 * @author Arunkumar passtoarun@gmail.com.
 */
let airportDetails = [
	{
		"code": "MAA",
		"name": "Chennai Airport",
		"city": "Chennai",
		"state": "TN",
		"country": "India",
	},
	{
		"code": "CJB",
		"name": "Coimbatore Airport",
		"city": "Coimbatore",
		"state": "TN",
		"country": "India",
	},
	{
		"code": "DEL",
		"name": "Indhira Gandhi Airport",
		"city": "Delhi",
		"state": "Delhi",
		"country": "India",
	},
	{
		"code": "COK",
		"name": "Cochin Airport",
		"city": "Kochi",
		"state": "Kerala",
		"country": "India",
	},
	{
		"code": "GOI",
		"name": "Goa Airport",
		"city": "Goa",
		"state": "Goa",
		"country": "India",
	}
];

/**
* @desc findAll - Return all airport details.
* @return Array - list of airports
*/
let findAll = () => {
	return airportDetails;
}

/**
* @desc getAirport - get airport details basedo on the airport code.
* @param String code - Airport code ex, MAA - chennai.
* @return Array - fitlered airport object
*/
let getAirport = (code) => {
	return airportDetails.filter((airport) => {
		return airport.code === code;
	});
};

export {
	findAll,
	getAirport
}