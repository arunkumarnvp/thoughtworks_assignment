/** 
 * @desc FlightListFactory - create the UI for list of flights.
 * @author Arunkumar passtoarun@gmail.com.
 * @required MesageService - returns the message lsit.
 */
import MessageService from './message-service.js';

/**
 * @desc showFlightList - create UI for list of flights returns as search result.
 * @param Array onweWayResult - oneway flight list.
 * @param Array returnResult - return flight list.
 * @param Boolean isRoundTrip - true, if it is round trip.
 * @return Boolean - true, if flight list has been created. false, otherwise.
 */
function showFlightList(onweWayResult, returnResult, isRoundTrip) {
    let searchListEle = document.querySelector('#flight-list');
    searchListEle.innerHTML = "";
    if (onweWayResult && onweWayResult.length === 0 && returnResult && returnResult.length === 0) {
        let message = MessageService.getMessage("E06");
        searchListEle.innerHTML = `<div class="message bold">${message}</div>`;
        return false;
    }
    if (onweWayResult && onweWayResult.length > 0) {
        searchListEle.innerHTML = `<div class="left">${createTravelEntry(onweWayResult, isRoundTrip)}</div>`
    }
    if (returnResult && returnResult.length > 0) {
        searchListEle.innerHTML = `${searchListEle.innerHTML}<div class="right">${createTravelEntry(returnResult, isRoundTrip)}</div>`;
    }
    return true;
}

/**
 * @desc createTravelEntry - create and return the list of flight UI.
 * @param Array list - flight list.
 * @return String - HTML to create list of flights
 */
function createTravelEntry(list, hasRoundTrip) {
    return list.map(flight => {
        return `
            <div class="flight">
                <div class="travel-info">
                    <div class="header-block">
                        <div class="flight-logo-block">
                            <img src="./images/${flight.logo}" alt="" class="flight-logo">
                        </div>
                        <div class="flight-block">
                            <span class="text-highlight">${flight.name}</span>
                            <span class=light>(${flight.category})</span>
                        </div>
                    </div>
                    <div class="details-block">
                        <div class="detail left">
                            <span class="bold">${flight.origin} <i class="fa fa-long-arrow-right" aria-hidden="true"></i> ${flight.destination}</span>
                            <span class="light-text">Depart : ${ moment(flight.departureTime).format("hh:mm A")}</span>
                            <span class="light-text">Arrive : ${ moment(flight.arrivalTime).format("hh:mm A")}</span>
                        </div>
                    </div>
                </div>
                <div class="price-info">
                    <div class="price-block">
                            <span class="price">Rs ${flight.price}</span>
                    </div>
                    <button class="btn cool">Book Now</button>
                </div>
            </div>`;
    }).join('');
}

/**
 * @desc showHeaderBlock - create search result header section.
 * @param Object origin - sorce details.
 * @param Object destination - to location details.
 */
function showHeaderBlock(origin, destination, searchPayload) {

    let isDisplay = (searchPayload.isRoundTrip) ? 'block' : 'none';

    // create left block
    let headerElem = document.querySelector('#conent-header');

    headerElem.innerHTML = `
        <div class="left">
            <span>${origin.city} <i class="fa fa-long-arrow-right" aria-hidden="true"></i> ${destination.city}</span>
            <span style="display:${isDisplay}">${destination.city} <i class="fa fa-long-arrow-right" aria-hidden="true"></i> ${origin.city}</span>
        </div>
        <div class="right">
            <div class="row">
                <span> Departure : </span><span class="bold">${moment(searchPayload.startDate).format('DD-MM-YYYY')}</span>
            </div>
            <div class="row" style="display:${isDisplay}">
                <span> Return : </span><span class="bold">${moment(searchPayload.returnDate).format('DD-MM-YYYY')}</span>
            </div>
            <div class="row">
                <span> Category : </span><span class="bold">${searchPayload.category}</span>
            </div>
        </div>`;
}

export default {
    showFlightList,
    showHeaderBlock
}