/** 
 * @desc Flight-Service - handles flight data.
 * @author Arunkumar passtoarun@gmail.com.
 * @required flightDetails - flight details
 * 
 */
import * as flightDetails from './flight-dataset.js';

/**
 * @desc getFlights - filter and return the list of flights based on the criteria.
 * @param Object paylod - search input.
 * @return Array - filtered list.
 */
let getFlights = (payload) => {
    let flightList = flightDetails.getFlightDetails().filter((flight) => {
        return flight.origin === payload.origin && flight.destination === payload.destination &&
            moment(payload.startDate).format('DD-MM-YYYY') === moment(flight.departureTime).format('DD-MM-YYYY');
    });
    if (payload.category === 'All')
        return flightList;
    return flightList.filter(flight => flight.category === payload.category);
}

export {
    getFlights
}