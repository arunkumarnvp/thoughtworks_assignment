/** 
 * @desc Message-service - maintains list for all the message text used in the applicaton
 * @author Arunkumar passtoarun@gmail.com.
 */
let messageList = [{
    id: 'E01',
    message: 'Please select origin loaction.'
},
{
    id: 'E02',
    message: 'Please select destination loaction.'
},
{
    id: 'E03',
    message: 'Please select different Origin and Destination.'
},
{
    id: 'E04',
    message: 'Please select departure date.'
},
{
    id: 'E05',
    message: 'Please select return date.'
},
{
    id: 'E06',
    message: 'No flights available. Sorry for the inconvenience!'
},
];

/**
* @desc getMessage - filter and return the message based on the id.
* @param String - id of the message.
*/
let getMessage = function(id) {
let msg = messageList.filter((msg) => {
    return msg.id === id;
});
return (msg.length > 0) ? msg[0].message : '';
};

export default {
getMessage
}