/** 
 * @desc createSearchBlock to handle search functionality.
 * @author Arunkumar passtoarun@gmail.com.
 * @required AirportService - to get airport details.
 * @required DataList - Reusable autofilter custom component.
 * 
 */
import * as AirportService from './airport-service.js';
import DataList from '../components/datalist.js';

/**
 * @desc createSearchBlock - search component UI creation.
 * @return Object - search input.
 */
export let createSearchBlock = function() {
    let isRoundTrip = false;
    let startDate = null;
    let returnDate = null;

    let travelType = document.querySelector('#travelType');
    travelType.addEventListener('click', function() {
        isRoundTrip = this.classList.toggle('active');
        let returnDateEle = document.querySelector('#returnDateBlock');
        returnDateEle.style.display = (isRoundTrip) ? "block" : "none";
    });

    let airportDetails = AirportService.findAll();
    let airportList = airportDetails.map(airport => {
        return {
            value: airport.code,
            text: `${airport.name}, ${airport.state}`
        }
    });

    const origin = new DataList("originList", airportList);
    const destination = new DataList("destinationList", airportList);

    // Departure Date
    new Lightpick({
        field: document.getElementById('deptDate'),
        minDate: moment().add(1, 'day'),
        format: "DD-MM-YYYY",
        autoclose: true,
        dropdowns: false,
        onSelect: function(date) {
            startDate = date;
        }
    });

    // Return  Date
    new Lightpick({
        field: document.getElementById('returnDate'),
        minDate: moment().add(1, 'day'),
        format: "DD-MM-YYYY",
        dropdowns: false,
        autoclose: true,
        onSelect: function(date) {
            returnDate = date;
        }
    });

    // Travel type
    const travelTypeList = ['All', 'Economy', 'Business', 'Elite'].map((type) => {
        return {
            value: type,
            text: type
        }
    });
    const travelCategory = new DataList("travelCategory", travelTypeList);

    /**
     * @desc closure - export the search input..
     * @return Object - search input.
     */
    return function() {
        return {
            isRoundTrip: (isRoundTrip) ? true : false,
            origin: (origin.selectedValue) ? origin.selectedValue.id : null,
            destination: (destination.selectedValue) ? destination.selectedValue.id : null,
            startDate: startDate,
            returnDate: (isRoundTrip) ? returnDate : null,
            category: (travelCategory.selectedValue) ? travelCategory.selectedValue.text : 'All'
        }
    }
}