/** 
 * @desc Search-Result-Factory to create search result component.
 * @author Arunkumar passtoarun@gmail.com.
 * @required AirportService - to get airport details.
 * @required FlightService - ligth list based on search criteria and filters.
 * @required DataList - Reusable autofilter custom component.
 * @required RangeSlider - Resuable slider component for filter.
 * @required FlightListFactory - create the UI for list of flights.
 * 
 */
import * as AirportService from './airport-service.js';
import * as FlightService from './flight-service.js';
import DataList from '../components/datalist.js';
import RangeSlider from '../components/range-slider.js';
import UtilService from './util-service.js';
import FlightListFactory from './flight-list-factory.js';

/**
 * @desc createSearchResult - create the search result page.
 * @param Object searchPayload - search input.
 */
let createSearchResult = function(searchPayload) {
    this.minPrice = 0;
    this.maxPrice = 60000;
    this.searchPayload = searchPayload;
    let welcomeBlock = document.querySelector('#welcome-block');
    welcomeBlock.style.display = 'none';

    let mainContent = document.querySelector('#main-content');
    mainContent.style.display = 'flex';


    let origin = AirportService.getAirport(searchPayload.origin)[0];
    let destination = AirportService.getAirport(searchPayload.destination)[0];
    FlightListFactory.showHeaderBlock(origin, destination, searchPayload);

    this.onweWayResult = FlightService.getFlights(searchPayload);
    this.returnResult = [];
    if (searchPayload.isRoundTrip) {
        let returnPayload = JSON.parse(JSON.stringify(searchPayload));
        returnPayload.origin = searchPayload.destination;
        returnPayload.destination = searchPayload.origin;
        returnPayload.startDate = searchPayload.returnDate;
        this.returnResult = FlightService.getFlights(returnPayload);
    }
    let isFlightsAvailable = FlightListFactory.showFlightList(this.onweWayResult, this.returnResult, searchPayload.isRoundTrip);
    let filterBlock = document.querySelector('#filter-block');
    if (!isFlightsAvailable) {
        filterBlock.innerHTML = '';
        return;
    }
    filterBlock.innerHTML = `
    <form>
        <div class="form-field">
            <label>Sort By:</label>
            <div id="sort-block" class="data-list">
                <input class="form-input" type="text" value="Select" autocomplete="off" readonly>
                <i class="icon iconfont icon-arrow"></i>
                <ul></ul>
            </div>
        </div>

        <div class="form-field">
            <label>Price:</label>
            <div id="price-slider"></div>
        </div>
    </form>
    `;

    /**
     * @desc onSortChanged - callback on sort selection change.
     * @param Object node - type of sort.
     */
    let onSortChanged = function(node) {
        this.onweWayResult = UtilService.getSortedList(this.onweWayResult, node.id);
        this.returnResult = UtilService.getSortedList(this.returnResult, node.id);
        this.onPriceChanged([this.minPrice, this.maxPrice]);
    }

    /**
     * @desc onPriceChanged - callback on sort slider change.
     * @param Array - max and min price value.
     */
    this.onPriceChanged = function(price) {
        [this.minPrice, this.maxPrice] = price;
        let oneWayList = UtilService.getFilteredList(this.onweWayResult, this.minPrice, this.maxPrice);
        let returnList = UtilService.getFilteredList(this.returnResult, this.minPrice, this.maxPrice);
        FlightListFactory.showFlightList(oneWayList, returnList, this.searchPayload.isRoundTrip);
    }

    new DataList("sort-block", UtilService.sortOptions, onSortChanged.bind(this));

    const app = document.querySelector('#price-slider');
    new RangeSlider({
        lowValue: 0,
        highValue: 60000,
        onChange: this.onPriceChanged.bind(this)
    }).appendToNode(app).addClass('slider');
}

export default {
    createSearchResult
}