/** 
 * @desc Utility service to re-use the repetitive methods 
 * @author Arunkumar passtoarun@gmail.com.
 * @required MesageService - returns the message lsit.
 */
import MessageService from './message-service.js';

/**
 * @desc getResult - Validate the search input.
 * @param Object payload - search input.
 * @return Object - valid or invalid with error message.
 */
let getResult = function(payload) {
    let result = {
        isValid: true,
    };

    if (!payload.origin) {
        result.isValid = false;
        result.msgCode = 'E01';
        return result;
    }

    if (!payload.destination) {
        result.isValid = false;
        result.msgCode = 'E02';
        return result;
    }

    if (payload.origin === payload.destination) {
        result.isValid = false;
        result.msgCode = 'E03';
        return result;
    }

    if (!payload.startDate) {
        result.isValid = false;
        result.msgCode = 'E04';
        return result;
    }

    if (payload.isRoundTrip && !payload.returnDate) {
        result.isValid = false;
        result.msgCode = 'E05';
        return result;
    }
    return result;
}

/**
 * @desc validateSearch - Validate, add error message if negative result.
 * @param Object payload - search input.
 * @return Object - valid or invalid with error message.
 */
let validateSearch = function(payload) {
    let result = getResult(payload);
    if (!result.isValid) {
        result.msg = MessageService.getMessage(result.msgCode);
    }
    return result;
};

/**
 * @desc getNumber - convert string with comma into number for price calculation.
 * @param String str - 10,000.00.
 * @return Integer - 10000.00.
 */
let getNumber = function(str) {
    return parseInt(str.replace(/,/g, ""));
}

/**
 * @desc getSortedList - sort the search result based on the input type.
 * @param Array - search result.
 * @param type - sort type - Cheapest, Early landing,.
 * @return list- sorted list.
 */
let getSortedList = function(list, type) {
    switch (type) {
        case 'CHEAP-PRICE':
            list.sort((a, b) => getNumber(a.price) < getNumber(b.price) ? -1 : getNumber(a.price) > getNumber(b.price) ? 1 : 0);
            return list;
        case 'EARLY-DEPARTURE':
            list.sort((a, b) => new Date(a.departureTime).getTime() < new Date(b.departureTime).getTime() ? -1 : new Date(a.departureTime).getTime() > new Date(b.departureTime).getTime() ? 1 : 0);
            return list;
        case 'LATE-DEPARTURE':
            list.sort((b, a) => new Date(a.departureTime).getTime() < new Date(b.departureTime).getTime() ? -1 : new Date(a.departureTime).getTime() > new Date(b.departureTime).getTime() ? 1 : 0);
            return list;

        case 'EARLY-LANDING':
            list.sort((a, b) => new Date(a.arrivalTime).getTime() < new Date(b.arrivalTime).getTime() ? -1 : new Date(a.arrivalTime).getTime() > new Date(b.arrivalTime).getTime() ? 1 : 0);
            return list;
        case 'LATE-LANDING':
            list.sort((b, a) => new Date(a.arrivalTime).getTime() < new Date(b.arrivalTime).getTime() ? -1 : new Date(a.arrivalTime).getTime() > new Date(b.arrivalTime).getTime() ? 1 : 0);
            return list;
    }
}


/**
 * @desc getFilteredList - filter result based on the input.
 * @param Array - search result.
 * @param min - minimum price.
 * @param max - maximum price.
 * @return Array - filtered result.
 */
let getFilteredList = function(list, min, max) {
    return list.filter((obj) => {
        let price = getNumber(obj.price);
        return price >= min && price <= max;
    })
}

/**
 * @desc sortOptions - list of options to sort the search result.
 */
let sortOptions = [{
        value: 'CHEAP-PRICE',
        text: 'Cheapest'
    },
    {
        value: 'EARLY-DEPARTURE',
        text: 'Early take off'
    },
    {
        value: 'LATE-DEPARTURE',
        text: 'Late take off'
    },
    {
        value: 'EARLY-LANDING',
        text: 'Early landing'
    },
    {
        value: 'LATE-LANDING',
        text: 'Late landing'
    }
];

export default {
    validateSearch,
    getSortedList,
    getFilteredList,
    sortOptions
}